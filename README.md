# Coronavirus Anxiety - How to Decrease It?

Coronavirus anxiety & mistakes of thinking
Exaggeration is one type of mistake of thinking that can increase coronavirus stress and anxiety That's when we amplify our ailments as when without much evidence we turn a common cold into the dreadful COVID-19 infection. Or perhaps overstate the possibilities of catching the infection by believing in terms of a higher likelihood than the data show.

Another mistake is leaping to conclusions. This error can amount to turning an innocuous piece of information into a catastrophe. Just because a liked one queue in a shopping line; it doesn't suggest they will not be observing social distancing. And even if they can not do this because of the behavior of others, they need not always get contaminated. If contaminated, they might not develop any symptoms or any major symptoms. Even if they, unfortunately, did become ill, it doesn't follow they will require hospitalization. Again, not all health center cases, unfortunately, pass away from the illness. To the extremely panicky person simply going to the shops can be equated with a high threat of death.

Another fault promoting coronavirus stress and anxiety is selectively taking care of one thing but neglect something else. Do we only discover negative news, and overlooking any positive aspects of the crisis? Just focusing on what is worrying and straining any reassuring patterns.

Furthermore, I can mention overgeneralization. For instance, if we presume that due to the fact that one member of our area dies of COVID-19, then all of us will have a serious danger of death too. This is overgeneralizing from the particular case to everyone.

Self-reflection & coronavirus stress and anxiety.
It's a good concept to capture ourselves out making these mistakes of thinking. However, it needs careful self-reflection, which you can accomplish with a run by yourself with a [running face mask](https://www.asgthestore.com.au/buy-face-masks-australia/). This is due to the fact that illogical thought is automated. So regular that it passes unobserved.

Mindfulness meditation can assist produce the needed self-awareness. Through self-reflection and meditation, we can end up being more able to observe our coronavirus stress and anxiety and the thoughts that accompany it in an unbiased way. Without hurrying to judgment but keeping a well-balanced perspective. Focusing awareness on the present moment, while calmly observing feelings, ideas, and bodily feelings.

This discipline allows one to take a psychological action back from what is going on around oneself. Then we can analyze our ideas in the light of day and challenge them if impractical. If we start searching for more practical mindsets, it ends up being possible to adopt a calmer attitude.

Somebody said, "Worrying doesn't eliminate tomorrow's troubles - It removes today's peace."

Underlying presumptions affecting coronavirus anxiety
When we stress, it is as if our company believes that by fretting about some occasion, we can in some way stop it happening. However, this is palpably untrue. It is among the unfavorable presumptions which need bringing out into the clear light of day. Just when it remains in the open can we start to challenge it. Otherwise, it will continue to run under the surface area causing damage.

To expose such underlying presumptions to daylight, we can use the potential that is integrated into mankind. This is the human power of rationality.

In one scene of the film Zulu, the native African's surround a little group of British soldiers at Rorke's Drift. They deal with being killed. A young private voices his fear and shock: "Why is it us, why us?" A sergeant examines, and replies, as if this were self-evident: "Because we're here lad." The skilled guy was not requesting for passive resignation but indicating that, when we are sensible about what is going on, then we give ourselves some sort of possibility to make the most of the circumstance by taking whatever action we can.

The logical mind states 'In the end we can only do what we can do. We can just get on with what is occurring today. With whatever activity we are associated with, or what task we are dealing with, or those challenges we are currently dealing with."

Some unhelpful beliefs
Our deeper beliefs are often hidden under the surface of our conscious awareness. Perhaps without realizing it, some individuals prevent all unpleasant or unwanted circumstances. They act as if they thought they must do this. The difficulty is the majority of us must deal with things that might fail to do with relationships, finance, health, work, and so on. We vary regarding how easily we cope with the possibility of problem and dissatisfaction. If we were to deal with the possibility of failure, of loss, and even discomfort, then we could consider the unknown future without insisting it follows our finest laid strategies.

So, who says we can not manage our feelings in difficult situations? Who states we will be successful in preventing hazardous and hazardous times? And who says we must discover order, certainty, and predictability in life?

In fact, we can't constantly have what we want. It does not matter what is our scenario in life, there is always an absence of understanding about what tomorrow will bring. Unpredictability has always been built into the fabric of life. And things not going as anticipated is inevitable. That's the case for all of us.

Isn't the obstacle of uncertainty a good thing? Yes, we all need a difficulty. It can keep us on our toes.